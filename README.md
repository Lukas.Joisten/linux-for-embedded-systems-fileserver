# Linux fileserver 

# How to run code
1. Set up a virtual environment (only has to be done once): `python -m venv lines-venv` or `python3 -m venv lines-venv`

2. Activate virtual environment: `source lines-venv/bin/activate` or `lines-venv\Scripts\Activate.ps1`

3. Install requirements: `pip install -r requirements.txt`

4. Run code: `python server.py`

5. Run `gunicorn -b 0.0.0.0:80 server:app`